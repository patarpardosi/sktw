/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'toska-muda': '#F2FAEF',
        'toska-tua': '#A7D8DC',
        'biru': '#487A9D',
        'biru-tua': '#243859',
        'merah': '#E63C49'
      }
    },
  },
  plugins: [],
}

